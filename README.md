# Angular + Objektorientierte Programmierung

## Dazugehörige Design Patterns
Constructor, Prototype

## Was habe ich davon?
Unterteilung eines langen Programms in Teilprogramme (Klassen), die wiederum in beliebig viele Teilprogramme aufgeteilt werden können (Methoden). Dennoch reicht alleine die Referenz einer Klasseninstanz (eines Objekts) bereits aus, um all ihre Teilprogramme nutzen zu können.

Weitere Vorteile:

 * Kapselung von Funktionalität, und damit Wiederverwendbarkeit und übersichtlicheren Quelltext
 * Dokumentierbarkeit
 * angenehmeres Debugging, da z.B. `console.log` mir zu jedem Objekt dessen Klassennamen anzeigt
 * automatisierte Testbarkeit

**Welche weiteren Vorteile bringt mir die objektorientierte Programmierung mit Klassen?**

## Klasse definieren
```javascript
function MyClass() {}
```
wobei `function` den Konstruktor darstellt.

Mit Parameterübergabe an den Konstruktor kann ich Initialwerte setzen:

```javascript
function MyClass(a, b, c) {
  this.a = a;
  this.b = b;
  this.c = c;
}
```

Methoden (objektgebunden via `this`):

```javascript
MyClass.prototype.myMethod = function(d) {
  return this.doSomethingWith(d);
};
MyClass.prototype.setE = function(e) {
  this.e = e;
};
MyClass.prototype.getE = function() {
  return this.e;
};
```

Statische Methoden (nicht objektgebunden, können aber neue Objekte erzeugen):

```javascript
MyClass.myStaticMethod = function(f) {
  return MyClass.doSomethingWith(f);
};
MyClass.fromFile = function(file) {
  var body = read(file);
  return new MyClass(body);
};
```

## Klasse verwenden: Objekt erzeugen
```javascript
var obj = new MyClass(a, b, c);
```

## Klasse verwenden: erben
```javascript
function MySubClass(g, h, i) {
  this.g = g;
  this.h = h;
  this.i = i;
}
MySubClass.prototype = Object.create(MyClass.prototype);
MySubClass.prototype.constructor = MySubClass;
```

`MySubClass` erbt von `MyClass`.

## Zusammenspiel mit Angular

### Vorschlag: Ein Service, eine Datei, eine Klasse

```javascript
angular.module('mymodule').factory('MyClass', [function() {

  function MyClass() {}

  // attach methods

  return MyClass;

}]);
```

### Alternativvorschlag: Service dient als Namespace

Innerhalb des Namespaces kann eine Klasse definiert werden oder es können darin auch mehrere Klassen definiert werden.

```javascript
angular.module('mymodule').factory('myclasses', [function() {

  var myclasses = {};

  myclasses.MyFirstClass = function() {};
  // attach methods

  myclasses.MySecondClass = function() {};
  // attach methods

  return myclasses;

}]);
```

## Weiterführende Recherche

 * Gibt es bessere Möglichkeiten der objektorientierten Programmierung? (eingebettet in Angular)

*Christian Niederreiter, 2016-04*
